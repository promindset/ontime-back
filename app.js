// Imports
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");

// Definded Methods
const app = express();


// Midlewares
app.use(cors())

app.get("/", (req, res) => {
    res.send("hello world!");
})

// Listening Port
app.listen(3000, () => {
    console.log("server started on port 3000")
})